libextutils-libbuilder-perl (0.08-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Thu, 06 Apr 2023 11:40:56 +0000

libextutils-libbuilder-perl (0.08-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Apply multi-arch hints. + libextutils-libbuilder-perl: Add Multi-Arch: foreign.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 20 Nov 2022 01:45:27 +0000

libextutils-libbuilder-perl (0.08-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: change GitHub/CPAN URL(s) to HTTPS.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Laurent Baillet ]
  * fix lintian file-contains-trailing-whitespace warning

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).
  * Remove constraints unnecessary since stretch:
    + Build-Depends: Drop versioned constraint on libmodule-build-perl.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Mon, 13 Jun 2022 21:54:04 +0100

libextutils-libbuilder-perl (0.08-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Mon, 13 Jun 2022 17:27:24 +0200

libextutils-libbuilder-perl (0.08-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Thu, 22 Apr 2021 15:21:04 +0200

libextutils-libbuilder-perl (0.08-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.08.
  * Drop spelling.patch, merged upstream.

 -- gregor herrmann <gregoa@debian.org>  Thu, 22 Oct 2015 22:38:08 +0200

libextutils-libbuilder-perl (0.07-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.07.
  * Bump debhelper compatibility level to 9.
  * Add a patch to fix a spelling mistake in the POD.

 -- gregor herrmann <gregoa@debian.org>  Sat, 17 Oct 2015 23:37:55 +0200

libextutils-libbuilder-perl (0.06-1) unstable; urgency=low

  * Team upload.

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * New upstream release.
  * Add debian/upstream/metadata
  * Drop quilt fragments from debian/{rules,control}.
    The package uses source format '3.0 (quilt)'. And there are no patches.
  * debian/copyright: update Upstream-Contact and license stanzas.
  * Build-Depend on Module::Build 0.42.
  * Mention module name in long description.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Sat, 27 Sep 2014 17:05:35 +0200

libextutils-libbuilder-perl (0.04-1) unstable; urgency=low

  [ Danai SAE-HAN ]
  * Initial release (Closes: #608080).

 -- Danai SAE-HAN (韓達耐) <danai@debian.org>  Mon, 27 Dec 2010 01:24:08 +0100
